# JIRA Service Desk Twitter integration example

This is an Atlassian Connect add-on which listens to a Twitter stream and 
creates Service Desk requests from them.

Read the tutorial here: [Guide - Creating JIRA Service Desk requests from Twitter](https://developer.atlassian.com/jiracloud/guide-creating-jira-service-desk-requests-from-twitter-40000561.html)

## How to run this

1. Ensure that you have the following:
	* A running node.js environment (v5.10.0 or later)
	* The Atlassian Connect Express (ACE) framework.
	* A development instance of JIRA Service Desk (if you need one, get one [here](http://go.atlassian.com/cloud-dev)) 
2. Edit config.json and fill in the `"twitter"` section
3. Run `npm install`
4. Run `node app.js` or `npm start`

To test this add-on on a Cloud instance, you will need to run it on a server
that is visible to your Cloud instance. If you are using the ACE framework, it will create an ngrok tunnel from your local server for you. 
Otherwise, see this page for instructions: [Developing locally](https://developer.atlassian.com/static/connect/docs/latest/developing/developing-locally-ngrok.html)

