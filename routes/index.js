'use strict';

const ServiceDeskClient = require('../lib/servicedesk-client');
const TwitterClient = require('../lib/twitter-client');

let tweetStream;

function createRequestFromTweet(serviceDeskClient, serviceDeskId, requestTypeId, tweet) {
    // Normally, you'd use the serviceDeskId and requestTypeId to get the
    // list of fields for the request-type, then populate them.
    // For now, let's just create a payload directly and use that.

    const requestPayload = {
        summary: tweet.text,
        description: 'Created via Twitter'
    };

    return serviceDeskClient.createRequest(serviceDeskId, requestTypeId, requestPayload);
}

module.exports = (app, addon) => {
    app.get('/', (req, res) => {
        res.format({
            'text/html': () => {
                res.redirect('/atlassian-connect.json');
            },
            'application/json': () => {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    app.post('/enabled', (req, res) => {
        const appKey = req.body.key;
        const clientKey = req.body.clientKey;

        const httpClient = addon.httpClient({
            appKey,
            clientKey
        });
        const serviceDeskClient = new ServiceDeskClient(httpClient);
        const twitterClient = new TwitterClient(addon.config.twitter());

        tweetStream = twitterClient.streamTweets({track: addon.config.twitter().query})
            .onTweetReceived(tweet => {
                console.log(tweet.text);
                createRequestFromTweet(serviceDeskClient,
                    addon.config.twitter().serviceDeskId,
                    addon.config.twitter().requestTypeId,
                    tweet);
            });
    });

    app.post('/disabled', (req, res) => {
        tweetStream.stop();
    });
};
