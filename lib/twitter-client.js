'use strict';

const Twit = require('twit');

const DEFAULT_TWITTER_CONFIG = {
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    access_token: process.env.TWITTER_ACCESS_TOKEN,
    access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
};

class TwitterClient {
    constructor(config) {
        this.twit = new Twit(config || DEFAULT_TWITTER_CONFIG);
    }

    streamTweets(options) {
        return new TweetStream(this.twit.stream('statuses/filter', options || {}));
    }
}

class TweetStream {
    constructor(twitStream) {
        this.twitStream = twitStream;
    }

    start() {
        this.twitStream.start();
        return this;
    }

    stop() {
        this.twitStream.stop();
        return this;
    }

    onTweetReceived(handler) {
        this.twitStream.on('tweet', handler);
        return this;
    }
}

module.exports = TwitterClient;
