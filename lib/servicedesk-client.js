'use strict';

function promiseOf(fn, arg) {
    return new Promise((resolve, reject) => {
        function callback(err, res) {
            if (err) {
                reject(err);
            } else if (res.statusCode < 200 || res.statusCode > 299) {
                reject({error: `${res.statusCode} ${res.statusMessage}`});
            } else {
                resolve(res.body);
            }
        }

        fn(arg, callback);
    });
}

class ServiceDeskClient {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }

    getServiceDesks() {
        return promiseOf(
            this.httpClient.get,
            {
                url: '/rest/servicedeskapi/servicedesk',
                json: true
            }
        );
    }

    getRequestTypes(serviceDeskId) {
        return promiseOf(
            this.httpClient.get,
            {
                url: `/rest/servicedeskapi/servicedesk/${serviceDeskId}/requesttype`,
                json: true
            }
        );
    }

    getRequestTypeFields(serviceDeskId, requestTypeId) {
        return promiseOf(
            this.httpClient.get,
            {
                url: `/rest/servicedeskapi/servicedesk/${serviceDeskId}/requesttype/${requestTypeId}/field`,
                json: true
            }
        );
    }

    createRequest(serviceDeskId, requestTypeId, requestFieldValues) {
        return promiseOf(
            this.httpClient.post,
            {
                url: `/rest/servicedeskapi/request`,
                body: {
                    serviceDeskId,
                    requestTypeId,
                    requestFieldValues
                },
                json: true
            }
        );
    }
}

module.exports = ServiceDeskClient;
